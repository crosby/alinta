# Alinta Code test

This repository contains a code test for Alinta. 

It is a console application which fetches a list of movies and the roles played by actors in the movie and outputs a list of actors and the characters they've played in the movies sorted by the film's name.

##### Response example

```
John Belushi
    John Blutarsky
    Jake Blues
```
(Note that the order is reversed as Animal House is alphabetically before The Blues Brothers)

##### Assumptions

- An actor line and associated character names are not shown when an actor doesn't have a name
- If a movie role has duplicates, they are merged together
- Actors are not sorted

## How to run?

In order to run this code, you first need to [install .NET Core](http://dotnet.github.io/getting-started/). After that, you can clone this repo, go into the folder and either:

* Run from source using the following commands:
	* `dotnet restore`
	* `dotnet run --project ./CodeTest/`
* Compile and run using the following commands
	* `dotnet restore`
	* `dotnet build`
	* `dotnet CodeTest/bin/Debug/netcoreapp2.0/CodeTest.dll`
* Run tests using the following commands
	* `dotnet restore`
	* `dotnet test`
