﻿using System.Linq;
using System.Collections.Generic;
using CodeTest.Models;

namespace CodeTest.Extensions
{
    public static class MovieExtensions
    {
        /// <summary>
        /// Groups movies by actor.
        /// </summary>
        public static IEnumerable<Actor> GroupByActor(this IEnumerable<Movie> movies) =>
            movies.SelectMany(l => l.Roles, (movie, role) => new { movie, role })
                .Select(selection => new
                {
                    Actor = selection.role.Actor,
                    ActorRole = new ActorMovieRole
                    {
                        MovieName = selection.movie.Name,
                        RoleName = selection.role.Name
                    }
                })
                .Where(s => !string.IsNullOrEmpty(s.Actor))
                .GroupBy(mr => mr.Actor)
                .Select(group => new Actor
                {
                    Name = group.Key,
                    MovieRoles = group
                        .Where(r => !string.IsNullOrEmpty(r.ActorRole.RoleName))
                        .GroupBy(rg => new { rg.ActorRole.MovieName, rg.ActorRole.RoleName })
                        .Select(r => new ActorMovieRole { MovieName = r.Key.MovieName, RoleName = r.Key.RoleName })
                        .OrderBy(r => r.MovieName)
                });
    }
}
