﻿using System.Collections.Generic;

namespace CodeTest.Models
{
    /// <summary>
    /// Represents a movie.
    /// </summary>
    public class Movie
    {
        /// <summary>
        /// Gets or sets the name of the movie
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets roles played by actors in the movie
        /// </summary>
        public IEnumerable<MovieRole> Roles { get; set; }
    }
}
