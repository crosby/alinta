﻿using System.Collections.Generic;

namespace CodeTest.Models
{
    public class Actor
    {
        /// <summary>
        /// Gets or sets the actor's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the roles the actor has played in movies
        /// </summary>
        public IEnumerable<ActorMovieRole> MovieRoles { get; set; }
    }
}
