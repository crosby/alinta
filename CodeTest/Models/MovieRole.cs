﻿using System.Collections.Generic;

namespace CodeTest.Models
{
    public class MovieRole
    {
        /// <summary>
        /// Gets or sets the name of the role in a movie
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the actor's name
        /// </summary>
        public string Actor { get; set; }
    }
}
