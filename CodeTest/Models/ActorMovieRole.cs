﻿using System.Collections.Generic;

namespace CodeTest.Models
{
    public class ActorMovieRole
    {
        public string MovieName { get; set; }
        public string RoleName { get; set; }
    }
}
