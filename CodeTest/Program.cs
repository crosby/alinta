﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeTest.Configuration;
using CodeTest.Extensions;
using CodeTest.Models;
using CodeTest.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace CodeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            AppConfiguration configuration = BuildConfiguration();
            ServiceProvider serviceProvider = BuildServiceProvider(configuration);

            ILogger logger = serviceProvider.GetService<ILoggerFactory>().CreateLogger<Program>();
            logger.LogDebug("Fetching movies");

            IMovieClient client = serviceProvider.GetService<IMovieClient>();

            try
            {
                IEnumerable<Actor> actors = client.GetMovies().GroupByActor();
                OutputActors(actors);
            }
            catch
            {
                Console.WriteLine("Error fetching movies.");
            }
        }

        static void OutputActors(IEnumerable<Actor> actors)
        {
            if (!actors.Any())
            {
                Console.WriteLine($"No actors found.");
            }

            foreach (var actor in actors)
            {
                Console.WriteLine(actor.Name);
                foreach (ActorMovieRole role in actor.MovieRoles)
                {
                    Console.WriteLine($"\t{role.RoleName}");
                }
            }
        }

        static AppConfiguration BuildConfiguration()
        {
            var configurationBuilder = new ConfigurationBuilder()
                .AddJsonFile("Config.json", optional: false);

            IConfigurationRoot configurationRoot = configurationBuilder.Build();

            AppConfiguration appConfiguration = configurationRoot.Get<AppConfiguration>();

            return appConfiguration;
        }

        static ServiceProvider BuildServiceProvider(AppConfiguration configuration)
        {
            ServiceProvider serviceProvider = new ServiceCollection()
                .AddLogging(builder => builder.SetMinimumLevel(LogLevel.Trace))
                .AddTransient<IRestClient, RestClient>()
                .AddTransient<IMovieClient>(
                    provider => new MovieClient(
                        new Uri(configuration.BaseUrl),
                        provider.GetService<IRestClient>(),
                        provider.GetService<ILoggerFactory>().CreateLogger<MovieClient>()))
                .BuildServiceProvider();

            serviceProvider
                .GetService<ILoggerFactory>()
                .AddConsole(LogLevel.Warning);

            return serviceProvider;
        }
    }
}