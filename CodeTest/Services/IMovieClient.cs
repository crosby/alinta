﻿using System.Collections.Generic;
using CodeTest.Models;

namespace CodeTest.Services
{
    interface IMovieClient
    {
        IList<Movie> GetMovies();
    }
}
