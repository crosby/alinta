﻿using System;
using System.Collections.Generic;
using System.Net;
using CodeTest.Models;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace CodeTest.Services
{
    public class MovieClient : IMovieClient
    {
        readonly ILogger logger;
        readonly IRestClient restClient;

        const int Milliseconds = 1;
        const int Second = Milliseconds * 1000;

        public MovieClient(Uri baseUrl, IRestClient restClient, ILogger logger)
        {
            this.restClient = restClient ?? throw new ArgumentNullException(nameof(restClient));
            this.restClient.BaseUrl = baseUrl ?? throw new ArgumentNullException(nameof(baseUrl));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public IList<Movie> GetMovies()
        {
            IRestRequest restRequest = new RestRequest("/movies", Method.GET);
            restRequest.AddHeader("Content-Type", "application/json");

            restRequest.Timeout = Second * 5;

            IRestResponse<List<Movie>> response = restClient.Execute<List<Movie>>(restRequest);

            if (response.ErrorException != null)
            {
                string message = string.Format("An error occured calling the movies URI");
                logger.LogCritical(message, response.ErrorException);
                throw new Exception(message, response.ErrorException);
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                string errorMessage = "Movies endpoint does not exist.";
                logger.LogCritical(errorMessage);
                throw new Exception(errorMessage, response.ErrorException);
            }

            return response.Data;
        }
    }
}
