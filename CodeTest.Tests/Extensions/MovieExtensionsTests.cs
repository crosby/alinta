﻿using System.Collections.Generic;
using System.Linq;
using CodeTest.Extensions;
using CodeTest.Models;
using Xunit;

namespace CodeTest.Tests.Extensions
{
    public class MovieExtensionsTests
    {
        List<Movie> GetTestMovies() {
            return new List<Movie> {
                new Movie { 
                    Name = "Beverly Hills Cop", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Axel Foley", Actor = "Eddie Murphy" }, 
                        new MovieRole { Name = "Billy Rosewood", Actor = "Judge Reinhold" }, 
                        new MovieRole { Name = "Sgt. Taggart", Actor = "John Ashton" },
                        new MovieRole { Name = "Jenny Summers", Actor = "Lisa Eilbacher" },
                        new MovieRole { Name = "Mikey Tandino", Actor = "" }
                    }
                },
                new Movie { 
                    Name = "Stand By Me", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Gorgie Lachance", Actor = "Wil Wheaton" }, 
                        new MovieRole { Name = "Chris Chambers", Actor = "River Phoenix" }, 
                        new MovieRole { Name = "Teddy Duchamp", Actor = "Corey Feldman" },
                        new MovieRole { Name = "Ace Merrill", Actor = "Keifer Sutherland" },
                        new MovieRole { Name = "The Writer", Actor = "Richard Dreyfuss" }
                    }
                },
                new Movie { 
                    Name = "Star Trek", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Romulan", Actor = "Wil Wheaton" }, 
                        new MovieRole { Name = "Kirk", Actor = "Chris Pine" }, 
                        new MovieRole { Name = "Nero", Actor = "Eric Bana" },
                        new MovieRole { Name = "Spock", Actor = "Leonard Nimoy" },
                        new MovieRole { Name = "Scotty", Actor = "Simon Pegg" },
                        new MovieRole { Name = "Amanda Grayson", Actor = "Winona Ryder" }
                    }
                },
                new Movie { 
                    Name = "Family Guy", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Meg Griffin", Actor = "Mila Kunis" }, 
                        new MovieRole { Name = "Meg Griffin", Actor = "Mila Kunis" }, 
                        new MovieRole { Name = "Chris Griffin", Actor = "Seth Green" },
                        new MovieRole { Name = "Luke Skywalker", Actor = "Seth Green" },
                        new MovieRole { Name = "Joe Swanson" },
                        new MovieRole { Name = "Lois Griffin", Actor = "Alex Borstein" }
                    }
                },
                new Movie { 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Dr Barry Wolfson", Actor = "Keifer Sutherland" }
                    }
                }
            };
        }
        
        [Fact]
        public void GroupByActor_ReturnsActors()
        {
            var movies = new List<Movie> {
                new Movie { 
                    Name = "Stand By Me", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Gorgie Lachance", Actor = "Wil Wheaton" }
                    }
                },
                new Movie { 
                    Name = "Star Trek", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Romulan", Actor = "Wil Wheaton" },
                        new MovieRole { Name = "Kirk", Actor = "Chris Pine" }
                    }
                }
            };

            IList<Actor> result = movies.GroupByActor().ToList();

            var expected = new List<Actor> { 
                new Actor { Name = "Wil Wheaton", MovieRoles = new List<ActorMovieRole> { new ActorMovieRole { MovieName = "Stand By Me", RoleName = "Gorgie Lachance" }, new ActorMovieRole { MovieName = "Star Trek", RoleName = "Romulan" } } },
                new Actor { Name = "Chris Pine", MovieRoles = new List<ActorMovieRole> { new ActorMovieRole { MovieName = "Star Trek", RoleName = "Kirk" }}}
            };

            Assert.Equal(2, result.Count);
            Assert.Equal(expected[0].Name, result[0].Name);
            Assert.Equal(expected[0].MovieRoles.ToList()[0].MovieName, result[0].MovieRoles.ToList()[0].MovieName);            
            Assert.Equal(expected[0].MovieRoles.ToList()[0].RoleName, result[0].MovieRoles.ToList()[0].RoleName);
            Assert.Equal(expected[0].MovieRoles.ToList()[1].MovieName, result[0].MovieRoles.ToList()[1].MovieName);            
            Assert.Equal(expected[0].MovieRoles.ToList()[1].RoleName, result[0].MovieRoles.ToList()[1].RoleName);
            Assert.Equal(expected[1].Name, result[1].Name);
            Assert.Equal(expected[1].MovieRoles.ToList()[0].MovieName, result[1].MovieRoles.ToList()[0].MovieName);            
            Assert.Equal(expected[1].MovieRoles.ToList()[0].RoleName, result[1].MovieRoles.ToList()[0].RoleName);
        }

        [Fact]
        public void GroupByActor_WithEmptyActorName_ExcludesActorInResults()
        {
            var movies = new List<Movie> {
                new Movie { 
                    Name = "Beverly Hills Cop", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Axel Foley", Actor = "Eddie Murphy" }, 
                        new MovieRole { Name = "Mikey Tandino", Actor = "" }
                    }
                },
            };

            IList<Actor> result = movies.GroupByActor().ToList();

            var expected = new List<Actor> { 
                new Actor { Name = "Eddie Murphy", MovieRoles = new List<ActorMovieRole> { new ActorMovieRole { MovieName = "Beverly Hills Cop", RoleName = "Axel Foley" }}}
            };

            Assert.Equal(1, result.Count);
            Assert.Equal(expected[0].Name, result[0].Name);
            Assert.Equal(expected[0].MovieRoles.ToList()[0].MovieName, result[0].MovieRoles.ToList()[0].MovieName);
            Assert.Equal(expected[0].MovieRoles.ToList()[0].RoleName, result[0].MovieRoles.ToList()[0].RoleName);
        }

        [Fact]
        public void GroupByActor_SortsCharactersByMovieName()
        {
            var movies = new List<Movie> {
                new Movie { 
                    Name = "ZZZ", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Gorgie Lachance", Actor = "Wil Wheaton" }
                    }
                },
                new Movie { 
                    Name = "AAA", 
                    Roles = new List<MovieRole> { 
                        new MovieRole { Name = "Romulan", Actor = "Wil Wheaton" }
                    }
                }
            };

            IList<Actor> result = movies.GroupByActor().ToList();

            var expected = new List<Actor> { 
                new Actor { 
                    Name = "Wil Wheaton", 
                    MovieRoles = new List<ActorMovieRole> { 
                        new ActorMovieRole { MovieName = "AAA", RoleName = "Romulan" }, 
                        new ActorMovieRole { MovieName = "ZZZ", RoleName = "Gorgie Lachance" } 
                    }
                },
            };

            Assert.Equal(1, result.Count);
            Assert.Equal(expected[0].Name, result[0].Name);
            Assert.Equal(expected[0].MovieRoles.ToList()[0].MovieName, result[0].MovieRoles.ToList()[0].MovieName);            
            Assert.Equal(expected[0].MovieRoles.ToList()[0].RoleName, result[0].MovieRoles.ToList()[0].RoleName);
            Assert.Equal(expected[0].MovieRoles.ToList()[1].MovieName, result[0].MovieRoles.ToList()[1].MovieName);            
            Assert.Equal(expected[0].MovieRoles.ToList()[1].RoleName, result[0].MovieRoles.ToList()[1].RoleName);
        }

        [Fact]
        public void GroupByActor_WithEmptyList_ReturnsEmptyList()
        {
            var movies = new List<Movie>();

            IList<Actor> result = movies.GroupByActor().ToList();

            var expected = new List<Actor> ();

            Assert.Equal(0, result.Count);
        }
    }
}
