﻿using System;
using System.Collections.Generic;
using System.Net;
using CodeTest.Models;
using CodeTest.Services;
using Microsoft.Extensions.Logging;
using Moq;
using RestSharp;
using Xunit;

namespace CodeTest.Tests.Services
{
    public class MovieClientTests
    {
        private MockRepository mockRepository;

        private readonly Mock<IRestClient> mockRestClient;
        private readonly Mock<ILogger> mockLog;

        public MovieClientTests()
        {
            mockRepository = new MockRepository(MockBehavior.Default);

            mockRestClient = mockRepository.Create<IRestClient>();
            mockLog = mockRepository.Create<ILogger>();
        }

        private MovieClient GetTarget()
        {
            return new MovieClient(new Uri("http://localhost"), mockRestClient.Object, mockLog.Object);
        }

        [Fact]
        public void MovieClient_InvalidArguments_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => new MovieClient(null, mockRestClient.Object, mockLog.Object));
            Assert.Throws<ArgumentNullException>(() => new MovieClient(new Uri("http://localhost"), null, mockLog.Object));
            Assert.Throws<ArgumentNullException>(() => new MovieClient(new Uri("http://localhost"), mockRestClient.Object, null));
        }

        [Fact]
        public void GetMovies_WhenResponseOk_ReturnsMovies()
        {
            // Arrange
            mockRestClient.Setup(mock => mock.Execute<List<Movie>>(It.IsAny<IRestRequest>()))
                .Returns(new RestResponse<List<Movie>>
                    {
                        StatusCode = HttpStatusCode.OK,
                        Data = new List<Movie> {new Movie {Name = "Tester"}}
                    });

            // Act
            var response = GetTarget().GetMovies();

            // Assert
            Assert.NotNull(response);
        }

        [Fact]
        public void GetMovies_WhenResponseNotFound_ThrowsException()
        {
            // Arrange
            mockRestClient.Setup(mock => mock.Execute<List<Movie>>(It.IsAny<RestRequest>()))
                .Returns(new RestResponse<List<Movie>>
                    {
                        StatusCode = HttpStatusCode.NotFound
                    });


            // Act
            // Assert
            Assert.Throws<Exception>(() => GetTarget().GetMovies());
        }
    }
}
